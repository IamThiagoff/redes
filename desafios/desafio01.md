TRABALHO - CAMADA DE ENLACE
Utilizando o GNS3 da VM disponibilizada:

1 - Elaborar uma topologia linear com 5 OpenVSwitches (não fechar o loop ainda!). Incluir dois hosts, em switches distintos, configurados com os IPs: 192.168.0.1 e 192.168.0.2. Utilizar o link entre os switches COM vlan trunk (802.1q, tagged). Utilizar como ID da vlan os 3 últimos dígitos do seu GRR.  Um host deve ser configurado no modo access (untagged) e o outro no modo trunk (tagged). Verificar que os hosts estão se comunicando através do comando ping. 

2 - Provocar e identificar um loop de camada 2.

3 - Configurar STP (spanning-tree) para evitar loop

4 - Descrever o status e a função de algumas portas para explicar como que o loop foi evitado com STP.

5 - Verificar o tempo de convergência habilitando e desabilitando o loop.

6 - Voltar ao passo 3 e alterar a configuração para RSTP (rapid spanning-tree, passo b)

7 - Criar um Link-Aggregation (802.3ad) entre pelo menos dois switches (ou todos!) com rápida convergência, LACP habilitado e balanceamento usando TCP. Verificar o seu funcionamento. 